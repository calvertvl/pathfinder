mkdir public
& 'C:\Program Files (x86)\Elm Platform\0.18\bin\elm-make.exe' `
  --yes src/Main.elm --output=public/index.html
& 'C:\Program Files\MiKTeX 2.9\miktex\bin\x64\pdflatex.exe' `
  -disable-installer `
  doc\paper.tex `
  -aux-directory=doc `
  -draftmode
& 'C:\Program Files\MiKTeX 2.9\miktex\bin\x64\pdflatex.exe' `
  -disable-installer `
  doc\paper.tex `
  -aux-directory=doc `
  -output-directory=public
