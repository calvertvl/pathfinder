module Tests.Graph.Search.Utility
    exposing
        ( paths
        , collapse
        , nodes
        , graphs
        )

import Test exposing (..)
import Expect exposing (Expectation)
import Fuzz exposing (float, list, map3)
import Graph as G exposing (Node, Edge)
import List as L
import Graph.Search.Utility as U exposing (Step(..))
import Graph.Types as T exposing (PathNode, PathEdge)


paths : Test
paths =
    let
        noSteps =
            test "A list of no steps has no shortest path" <|
                \_ ->
                    U.findShortestPath []
                        |> Expect.equal []

        noFinalState =
            test "A list without a final state has no shortest path" <|
                \_ ->
                    let
                        ss =
                            [ InitialState G.empty
                            ]
                    in
                        U.findShortestPath ss
                            |> Expect.equal []

        multiFinalState =
            test "A list with multiple final states returns only the first" <|
                \_ ->
                    let
                        first =
                            [ [ 1, 2, 3 ] ]

                        second =
                            [ [ 1, 2, 3 ], [ 1, 2, 5 ] ]

                        firstFS =
                            FinalState first

                        secondFS =
                            FinalState second
                    in
                        U.findShortestPath [ firstFS, secondFS ]
                            |> Expect.equal first

        oneShortestPath =
            test "A list with a single shortest path returns it" <|
                \_ ->
                    let
                        path =
                            [ [ 1, 2, 3 ] ]

                        fs =
                            FinalState path
                    in
                        U.findShortestPath [ fs ]
                            |> Expect.equal path

        multipleShortestPaths =
            test "A list with multiple shortest paths returns all of them" <|
                \_ ->
                    let
                        path =
                            [ [ 1, 2, 3 ], [ 1, 2, 5 ] ]

                        fs =
                            FinalState path
                    in
                        U.findShortestPath [ fs ]
                            |> Expect.equal path
    in
        describe "Path Retrieval"
            [ noSteps
            , noFinalState
            , multiFinalState
            , oneShortestPath
            , multipleShortestPaths
            ]


collapse : Test
collapse =
    let
        noCollapse =
            test "An empty list collapses to an empty list" <|
                \_ ->
                    U.collapseSteps 1 []
                        |> Expect.equal []

        noInitialState =
            test "Collapsing a list with no initial state is an empty list" <|
                \_ ->
                    U.collapseSteps 1 [ FinalState [ [ 1, 2, 3 ] ] ]
                        |> Expect.equal []

        onlyInitialState =
            test "If list only has an initial state, we get it back" <|
                \_ ->
                    U.collapseSteps 1 [ InitialState G.empty ]
                        |> Expect.equal [ InitialState G.empty ]
    in
        describe "Step Collapse"
            [ noCollapse
            , noInitialState
            , onlyInitialState
            ]


graphs : Test
graphs =
    let
        noGraph =
            test "If we don't have a step, we get an empty graph" <|
                \_ ->
                    U.getGraphFromState Nothing
                        |> Expect.equal G.empty

        badNode =
            test "If we don't have the right kind of step, we get an empty graph" <|
                \_ ->
                    let
                        step =
                            Just <| FinalState [ [ 1, 3, 5 ] ]
                    in
                        U.getGraphFromState step
                            |> Expect.equal G.empty

        graphIdentity =
            test "If we have the right kind of step, we get back exactly the graph used to create it" <|
                \_ ->
                    let
                        graph =
                            U.sampleG

                        step =
                            Just <| InitialState graph
                    in
                        U.getGraphFromState step
                            |> Expect.equal graph
    in
        describe "Graph Retrieval"
            [ noGraph
            , badNode
            , graphIdentity
            ]


nodes : Test
nodes =
    let
        noPathNodes =
            test "A list of no path nodes produces an empty list" <|
                \_ ->
                    U.toNodes []
                        |> Expect.equal []

        pathConstantLength =
            test "Converting a list of path nodes does not change the length" <|
                \_ ->
                    let
                        nodes =
                            [ T.pNode 0 0 0
                            , T.pNode 1 1 1
                            , T.pNode 2 2 2
                            ]

                        convertedNodeCount =
                            U.toNodes nodes |> L.length
                    in
                        L.length nodes
                            |> Expect.equal convertedNodeCount

        pathOrder =
            test "Converting a list of path nodes does not change the order" <|
                \_ ->
                    let
                        pn1 =
                            T.pNode 0 0 0

                        pn2 =
                            T.pNode 1 1 1

                        pn3 =
                            T.pNode 2 2 2

                        pnodes =
                            [ pn1, pn2, pn3 ]

                        n1 =
                            G.Node 1 pn1

                        n2 =
                            G.Node 2 pn2

                        n3 =
                            G.Node 3 pn3

                        nodes =
                            [ n1, n2, n3 ]
                    in
                        U.toNodes pnodes
                            |> Expect.equal nodes

        noEdgeNodes =
            test "A list of no edge nodes produces an empty list" <|
                \_ ->
                    U.toEdges [] [] []
                        |> Expect.equal []
    in
        describe "Node conversion helpers"
            [ describe "Path conversion"
                [ noPathNodes
                , pathConstantLength
                , pathOrder
                ]
            , describe "Edge conversion"
                [ noEdgeNodes
                ]
            ]
