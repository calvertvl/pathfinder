module Tests.Graph.Search.Dijkstra exposing (all)

import Test exposing (..)
import Expect exposing (Expectation)
import Fuzz exposing (float, list, map3)
import Graph as G exposing (Node, Edge)
import List as L
import Graph.Search.Dijkstra as D
import Graph.Search.Utility as U exposing (toNodes, edgesFromNodesWithin, infinity)
import Graph.Types as T exposing (PathNode, PathEdge, NodeState(..), EdgeState(..))


all : Test
all =
    let
        emptyGraph =
            test "An empty graph has no paths" <|
                \_ ->
                    D.findShortestPath 1 2 G.empty
                        |> Expect.equal []

        singleNode =
            test "A single-node graph has no paths" <|
                \_ ->
                    let
                        nodes =
                            [ Node 0 <| T.pNode 0.0 0.0 0.0
                            ]

                        graph =
                            G.fromNodesAndEdges nodes []
                    in
                        D.findShortestPath 1 2 graph
                            |> Expect.equal []

        disjointGraph =
            test "A disjoint graph has no paths" <|
                    \_ ->
                        let
                            nodes =
                                toNodes U.sampleN

                            graph =
                                G.fromNodesAndEdges nodes []
                        in
                            D.findShortestPath 1 2 graph
                                |> Expect.equal []

        twoNodes =
            test "A connected two-node graph has one path" <|
                \_ ->
                    let
                        nodes =
                            toNodes
                                [ T.pNode 0.0 0.0 0.0
                                , T.pNode 1.0 1.0 1.0
                                ]

                        edges =
                            edgesFromNodesWithin 2.0 nodes

                        graph =
                            G.fromNodesAndEdges nodes edges
                    in
                        D.findShortestPath 1 2 graph
                            |> Expect.equal [ 1, 2 ]
    in
        describe "Dijkstra Search"
            [ emptyGraph
            , singleNode
            , twoNodes
            , disjointGraph
            ]
