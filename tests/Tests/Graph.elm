module Tests.Graph exposing (all)

import Test exposing (..)
import Tests.Graph.Search.Dijkstra as Dijkstra
import Tests.Graph.Search.Utility as Utility


all : Test
all =
    describe "Graph Traversal Algorithms"
        [ Dijkstra.all
        , describe "Utility Functions and Datatypes"
            [ Utility.nodes
            , Utility.graphs
            , Utility.paths
            , Utility.collapse
            ]
        ]
