module Tests exposing (suite)

import Test exposing (..)
import Tests.Graph


import Test.Runner.Html

main : Test.Runner.Html.TestProgram
main =
    suite |> Test.Runner.Html.run

suite : Test
suite =
    describe "pathfinder"
        [ Tests.Graph.all
        ]
