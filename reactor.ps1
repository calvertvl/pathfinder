elm-test --add-dependencies .\tests\elm-package.json
Start-Process -WorkingDirectory .        elm-reactor -ArgumentList "--port=8000"
Start-Process -WorkingDirectory .\tests\ elm-reactor -ArgumentList "--port=8080"
elm-test --watch
