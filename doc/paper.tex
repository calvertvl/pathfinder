\documentclass[titlepage]{article}
\usepackage{hyperref}
\usepackage[T1]{fontenc}
\usepackage{endnotes}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{amsmath}

\let\footnote=\endnote

\begin{document}

\begin{titlepage}
  \center 
  \textsc{\Large ECSE 4500: Distributed Systems \\ and Sensor Networks} \\[1.0 cm]
  \textsc{\Large ECSE 4750: Computer Graphics} \\
  \vfill
  {\huge\bfseries Interactive Graph Search Algorithms for Routing and
    Pathfinding} \\
  \vfill
  {\large Victor Calvert} \\
  {\large\today}
\author{Victor Calvert}
\end{titlepage}

\tableofcontents

\section{Introduction}

This project is intended to provide a framework for the interactive exploration of
graph search algorithms\footnote{Dijkstra, A*, Bellman-Ford, etc.} for purposes
of comparison both between algorithms and across different types of data sets.
It was inspired by some of the online sorting algorithm
comparisons,\footnote{
  see e.g. \url{https://www.toptal.com/developers/sorting-algorithms}
  and \\ \url{http://cs.usfca.edu/~galles/visualization/ComparisonSort.html}
}
as well as a desire to see similar techniques
properly applied to graph search.

There are multiple uses for this sort of framework, but the initial design
intent was to provide a suitable educational tool, for example to be used in an
educational setting, while not preventing future extensions that would encourage
use as a design tool for engineering sensor networks, exploring pathfinding
algorithms for autonomous robots or game AI, and other unforeseen educational or
design uses.

\section{Design}

This framework is intended to allow exploration of graph search algorithms in an
interactive manner. More precisely, the user should be able to step through an
algorithm's execution against a data set and see the current state of the
algorithm after each step.

As part of the value of the interaction is in being able to see what happened
before a given point, the ability to move bidirectionally in the operation
sequence is invaluable.

Designing the underlying data structures to support an operation sequence that
allows this bidirectional movement, and the associated tools to provide the
state at each step of the way, was a major undertaking. Each operation needed by
the algorithm had to be identified, along with any associated parameters. In
this case, this is currently limited to the nodes and edges of the graph, with
some simple metrics. In most cases, algorithm-specific state is not needed, but
some algorithms, such as Floyd-Warshall\footnote{ \url{
https://en.wikipedia.org/wiki/Floyd\%E2\%80\%93Warshall_algorithm} } are easier to
follow with an adjacency matrix representation.

Each operation in Dijkstra's Algorithm affects one or more nodes. When the
operation stream is processed for viewing, each operation type places a node in
a specific state. These states are then mapped to colors. Due to the functional
language paradigm used, the colors and their meanings are mapped in one place.
For ease of understanding, the geometry of the nodes is defined in the same
source file.
Alternate mappings, for example for colorblind users, could be added, and
trivially switched at runtime. This decomposition of structure and visual
representation is similar to that seen with markup languages; a common example
of this is HTML and CSS. Other familiar applications more closely related would
be, for example, material properties in CAD programs and 3D modeling software.

Edges have fewer required states at this time, as Dijkstra's Algorithm is
node-centric. Both the edge geometry and the mapping between edge state and
color are defined in a single source file, as was done for nodes.
Implementations of additional algorithms may require additional
edge states.

Beyond the definition of the data structures themselves, a series of operations
on the algorithmic steps and graph state needed to be defined in order for them
to be useful. The actual process, after an initial attempt at defining
appropriate data structures, was to begin writing functions to execute these
operations, and to iteratively redesign the data structures when the module API
began to become cumbersome or repetetive.

\section{Algorithm Operations}
In the code, these are referred to as \texttt{Step}s.

There are two position-specific steps: \texttt{InitialState}, which provides the
starting graph structure, and \texttt{FinalState}, which provides the shortest
path(s) as node sequences. If there is no path, \texttt{FinalState} has an empty
list of paths.

The general-purpose steps are: \texttt{CurrentNode}, which is the current node
being investigated by the algorithm; \texttt{Neighbors}, which
indicates an operation finding the neighbors of the current node;
\texttt{Compare}, which indicates a comparison of node costs (or other
properties); \texttt{UpdateNodeCost}, which is an update operation on the graph;
and \texttt{MarkVisited}, which indicates that the algorithm has finished
processing this node (for Dijkstra; other algorithms may visit a node multiple
times).

There is also one general-purpose step implemented for purposes of simultaneus
operations, whether multi-core or distributed system processing, encapsulating
a list of steps executed at the same time: \texttt{GeneralStep}. As
all operations currently execute in unit time, and there are effectively no
communication delays, this would work well for a multi-core implementation. Some
additional data or repeated nesting will be needed to support proper distributed
networks due to communication delays and variable-length steps or step chains.

\section{General Operations}
In the code, these are referred to by name. The naming convention is generally
to start with a verb, then any adjectives or nouns needed to define the
operation. Type signatures also provide syntactic hints. In the case of boolean
results, however, the naming convention is usually \texttt{is<adjective>}.

In addition to those explained below, there are additional convenience functions to
handle conversion between datatypes and give friendly names to operations that
are expected to be repeated frequently.

\subsection{makeFinalState}
Given the start and ending nodes, and a graph for which the lowest cost node(s) to
reach the source is(are)
indicated at each node, this function identifies all possible paths of the same
(lowest) cost.

The graph itself is created by the search algorithm implementation; this is a
utility function to synthesize the final information for display of the end
state.

\subsection{findShortestPath}
This function extracts the \texttt{FinalState} step (which is always the last
step in the sequence) from a sequence of steps, and returns a list of node
sequences.

\subsection{getGraphFromState}
Similar operation to \texttt{findShortestPath}, except that this retrieves the
graph from an \texttt{InitialState} step.

\subsection{collapse}
This function effectively decodes steps into graph transformations, and applies
them to the graph provided. Proper functioning means that this function should
never see the \texttt{InitialState} step.

Primarily, this maps a step onto the \texttt{NodeState} type, which is how we
get colors in the visual representation.
\subsection{collapseSteps}
This function is the primary wrapper for iterative calling of \texttt{collapse},
and also handles cases where the list of steps cannot be collapsed, or is already
collapsed.

\begin{figure}[h!]
  \centering
  \begin{subfigure}[b]{0.4\linewidth}
    \includegraphics[width=\linewidth]{images/dijkstra1.png}
    \caption{\texttt{CurrentNode 1}}
  \end{subfigure}
  \begin{subfigure}[b]{0.4\linewidth}
    \includegraphics[width=\linewidth]{images/dijkstra2.png}
    \caption{\texttt{Neighbors 2 3 4}}
  \end{subfigure}
  \begin{subfigure}[b]{0.4\linewidth}
    \includegraphics[width=\linewidth]{images/dijkstra3.png}
    \caption{\texttt{Compare 1 2}}
  \end{subfigure}
  \begin{subfigure}[b]{0.4\linewidth}
    \includegraphics[width=\linewidth]{images/dijkstra4.png}
    \caption{\texttt{UpdateNodeCost 1 2}}
  \end{subfigure}
  \caption{First four steps of Dijkstra's Algorithm}
  \label{fig:dijkstra}
\end{figure}

An example of how the first 4 steps map onto the graph is in Figure
\ref{fig:dijkstra}. Note that the colors used to show the active node, neighbor
nodes, and update node are differently lit and shaded (though somewhat difficult
to see). Nodes being compared are a more obvious green.

\begin{figure}[h]
  \includegraphics{images/edgebug.png}
  \caption{The edge location bug}
  \label{fig:edgebug}
\end{figure}


\subsection{edgesFromNodesWithin}
This function takes a list of nodes and processes it once in a recursive manner
to identify all edges between nodes, given a maximum distance between nodes
(edge length).

Note that each node knows where it is in three-dimensional space, as part of the
node data structure stores this information.

This function also has the logic to pre-calculate transformations for the
standard edge geometry relative to the world coordinate system. There was a bug,
as seen in Figure \ref{fig:edgebug}. The bug was due to applying the scale,
translation, and rotation operations as Scale-Translate-Rotate, instead of
Scale-Rotate-Translate. See Figure \ref{fig:edgefixed} for the correct view.

The scaling step sets the length of the edge rod to the distance between the two
nodes, while translation moves the start point (at the origin) to the location
of one of the nodes. The rotation matrix is calculated to place the other end of
the rod at the destination node once the translation has occurred, using the
following equations to obtain the axis and angle from our two points $\vec{a}$
and $\vec{b}$:

\begin{align*}
    \vec{l} &= \Arrowvert \vec{b} - \vec{a}) \Arrowvert \\
    \vec{u} &= \vec{l} \times \hat{k} \\
    \theta &= \arccos{\vec{l} \cdot \vec{k}}
\end{align*}

Note that $\vec{a}$ is our starting point, relative to the origin, and $\vec{l}$
is the direction of $\vec{b}$ from $\vec{a}$ (normalized to a unit vector);
$\vec{u}$ is the axis of rotation, also a unit vector. The unit vector $\hat{k}$
is the positive z-axis, which is the original orientation of our edge model.

\begin{figure}[h]
  \includegraphics{images/edgefixed.png}
  \caption{The correct edge locations}
  \label{fig:edgefixed}
\end{figure}

\subsection{updateNodeCost}
This function is a node manipulation function that updates the cost of
travelling to a given node from a prior node. If multiple prior nodes have the
same cost, they are tracked as well.

Note that there is also a case here to alow for leaving the list of cheapest
prior nodes empty: this is needed when setting the source node's cost to 0, as
otherwise some form of infinte loop or null path condition would occur.

\subsection{updateNodeState}
This function updates the state of a given node; it is basically a utility
function to handle the nested data structures.

\subsection{resetNodeCost}
This function resets all node costs to positive infinity. For visit-once
algorithms, such as Dijkstra's Algorithm, as all neighboring nodes are assigned
a finite cost, this means that all nodes known to be connected to the source
node will be sorted first. In addition, upon reaching the destination or
exhausting all finite-cost nodes, it is then known that the infite-cost nodes
are not connected to the source, though the number of disconnected subgraphs is
unknown (but at least one disconnected subgraph exists if there are any nodes
with infinite cost).

\section{Graphics}

The vertex and pixel shaders are slightly modified from those used in Edward
Angel's \texttt{teapot5} program.\footnote{The copy used can be found at \\
  \url{https://wrf.ecse.rpi.edu/Teaching/graphics/SEVENTH_EDITION/CODE/11/teapot5.html}}
A basic flat-shaded model is used with per-triangle surface normals. This allows
for a simple graphical style, reminiscent of those commonly used for forensic
reconstructions, among others.

Colors were chosen per edge and per node to represent the different possible
states represented in the algorithm. See Figure \ref{fig:dijkstra} for an
example of node colors for a) active, b) neighbor, c) compare, and d) update.

\subsection{Geometry Processing: \texttt{entityHelper}}

As the graph state is effectvely a \emph{scene graph} for this program, we need
to transform it into the appropriate geometry for WebGL to render. This is done
by the \texttt{entityHelper} function found in \texttt{Main.elm}.

In effect, this function calculates the current state of the scene graph from
the current step and the list of steps, then transforms this state into
graphical primitives.

The code itself is simple: there is a helper function to calculate the
translation matrix and generate the WebGL \texttt{Entity}, which is a data
structure with mesh information, uniforms, and shaders. This helper function is
called by two other helper functions, one for nodes and one for edges. Each of
these maps the node/edge state to a color, and passes the appropriate mesh and
uniforms to the first helper function.

The actual logic is to generate a list of WebGL \texttt{Entity} records with the
edges first, and then the nodes, in effect concatenating the results of two map
operations (over the graph edges and nodes, respectively).

Nodes and Edges are defined in the \texttt{Pathfinder.Geometry.Node} and \\
\texttt{Pathfinder.Geometry.Edge} modules, respectively. This includes the
geometry itself, as well as the colors selected to represent the different
states. Changing the theme would only require updating these modules. Providing
the user wih the ability to update the theme, or implementing multiple
pre-defined themes, would require some more wide-ranging changes because of the
type signatures and lack of global data, but is a relatively straightforward
extension.

\subsection{Interaction}
User interaction is currently by means of buttons, and allows control of the
rotational view and the displayed graph state.

The rotational view is controled by the start and pause operations and the axis
selection buttons. The start and pause operations
simply change an internal flag.
The axis buttons change whch axis is
stored for rotation.

Rotations
are performed about the currently-stored axis on each time increment,
incrementing the angle by one degree. The frame rate is currently set at 60
frames per second (see \texttt{subscriptions} in \texttt{Main.elm}).

The step operations increment and decrement a step counter, which is used by the
\texttt{entityHelper} function to determine the state of the graph.

Updates to the displayed view are performed after each event message received by
the code. These events are the user interactions, and each time interval.

\section{Dijkstra's Algorithm Implementation}

Implementation of Dijkstra's Algorithm using the framework described above was
relatively straightforward. Due to the use of the Elm language, it required
translation from the standard iterative form. Translating procedural pseudocode
into a functional form is relatively difficult, as each loop becomes a recursive
function call, variables are immutable, and so on.

The easiest method was actually to take a description of what the algorithm is
doing at each step.\footnote{
  See Wikipedia, Dijkstra's Algorithm, the Algorithm section. \\
  \url{https://en.wikipedia.org/wiki/Dijkstra's_algorithm\#Algorithm}
} This gives a much better understanding of what is being done, which makes it
much easier to implement each step as a function (or subfunction). Assmebling
these functions with an appropriate conditional switch to continue recursion or
terminate is then relatively straightforward.


\section{Conclusion}

Creating the underlying data structures was a substantial undertaking, and
technically it is not complete, however it is sufficient for a solid proof of
concept and should form a solid basis for iterative development moving forward.
Building the associated geometry and the transformations to display
the graph structure in three dimensions was somewhat more straightforward, but
more difficult to debug when things went wrong.

The graphical elements themselves were not too difficult to link together with
the data structures, once the data structures were correct; those were left
until much closer to the end as the goal was a useful tool, and simple,
effective graphics are much more valuable for such usage when paired with solid
underlying logic than eye candy paired with no functioal code.

Developing this with a strongly-typed functional language that compiles to
JavaScript made the project much easier: I would not have wanted to attempt to
do this directly in JavaScript, as it is not possible to prevent impossible
states without a great deal more work. On the other hand, having primarily used
procedural languages in the past, the biggest pitfall was the desire to properly
unit test the code, resulting in unit tests that provided minimal value rather
than guaranteeing proper function.

\section{Discussion}

There are still some bugs that need to be resolved before the minimial
functionality is fully working, but aside from the edge transformation bug,
those should be relatively straightforward.

Future extensions and additional functionality will be built; this is an
interesting project to work with. There is also a great deal of scope for
extension, see Appendix A for a number of already-identified options beyond the
implementation of additional graph search algorithms. Many of
these are more in the realm of making this useful as an engineering tool, as
with the minimal functionality in place, and multiple algorithms implemented,
this will already be quite useful for educational purposes.

\section{Appendix A: Future Extensions}
Future extensions to support the intended use cases are:
\begin{itemize}
  \begin{item}
    A summary of the number of each type of operation required to compute
    the final result
  \end{item}
  \begin{item}
    User-defined operation timing
  \end{item}
  \begin{item}
    Costing algorithms in addition to three-dimensional distance,
    including inverse-square propagation
  \end{item}
  \begin{item}
    Node operation costs
  \end{item}
  \begin{item}
    Node energy budgets
  \end{item}
  \begin{item}
    Non-homogeneous nodes in a single graph
  \end{item}
  \begin{item}
    Moving nodes
  \end{item}
  \begin{item}
    Distributed graph search algorithms
  \end{item}
  \begin{item}
    Parametric pseudorandom generation of node distributions
  \end{item}
  \begin{item}
    Ability to save node distribution generations, e.g. to demonstrate a
    pathological case
  \end{item}
  \begin{item}
    Ability to export node distributions, e.g. for sharing with 
    co-workers, students, or others as an example case, or as the basis for
    further research in another environment
  \end{item}
  \begin{item}
    Ability to import node distributions, e.g. for use to compare algorithms for
    use in a network with a known node distribution.
  \end{item}
  \begin{item}
    Extension to use a server-side process via WebSockets to generate the step
    list, allowing for more complex simulation use (e.g. by using Erlang or
    Elixir to simulate actual node behavior).
  \end{item}
\end{itemize}

\section{Appendix B: Links and Reference Works}

\begin{itemize}
  \begin{item}
    The Elm Language, \url{http://elm-lang.org/}
  \end{item}
  \begin{item}
    Package documentation for the various packages used in the source code.
    \url{http://package.elm-lang.org}
  \end{item}
  \begin{item}
    Wikipedia, various articles on graph-related algorithms, in particluar \\
    \url{https://en.wikipedia.org/wiki/Dijkstra's_algorithm}
  \end{item}
\end{itemize}

\section{Appendix C: Code}
\begin{itemize}
  \begin{item}
    Full source code is available at
    \url{https://gitlab.com/calvertvl/pathfinder/}
  \end{item}
  \begin{item}
    An interactive version of the latest code is available at \\
    \url{https://calvertvl.gitlab.io/pathfinder/}
  \end{item}
\end{itemize}

\section{Appendix D: User Guide}

Open the page in a web browser that supports WebGL.

It is suggested that you use 
\url{https://calvertvl.gitlab.io/pathfinder/}
if you are not working on an extension or a patch.

As all search algorithms execute a sequence of operations, this program provides
controls to step through the execution of an algorithm to see the individual
steps.

At this time, only Dijkstra's Algorithm is implemented.

\begin{figure}[h]
  \includegraphics{images/buttons.png}
  \caption{The interaction buttons}
  \label{fig:buttons}
\end{figure}
The buttons along the top of the page, seen in Figure \ref{fig:buttons}, should be reasonably self-explanatory, but they are
described below. At this time, there are no keyboard shortcuts.
\begin{itemize}
  \begin{item}
    \textbf{Start}: Starts rotating around the selected (world) axis.
  \end{item}
  \begin{item}
    \textbf{Pause}: Stops rotating around the selected (world) axis; does not
    reset the position.
  \end{item}
  \begin{item}
    \textbf{X-Axis Rotation}: Selects the world x-axis as the axis of rotation
    (this is the default).
  \end{item}
  \begin{item}
    \textbf{Y-Axis Rotation}: Selects the world y-axis as the axis of rotation
  \end{item}
  \begin{item}
    \textbf{Z-Axis Rotation}: Selects the world z-axis as the axis of rotation
  \end{item}
  \begin{item}
    \textbf{<- Step <-}: Step backwards in the operation sequence.
  \end{item}
  \begin{item}
    \textbf{-> Step ->}: Step forwards in the operation sequence.
  \end{item}
\end{itemize}

Known limitations:
\begin{itemize}
  \begin{item}
    If you wish to reset the rotational state, you will need to reload the page.
  \end{item} 
  \begin{item}
    Edges currently are not colored.
  \end{item} 
\end{itemize} 

\section{Appendix E: Setting up a Development Environment}

In general, this requires installing two node.js packages. There is some
variation on different environments, but it is a good idea to start with the Elm
installation process, at \url{https://guide.elm-lang.org/install.html}. The
NPM-based process is suggested, as this allows easy installation of
\texttt{elm-test}.

I use Spacemacs\footnote{\url{http://spacemacs.org/}} with the \texttt{elm},
\texttt{git}, and \texttt{latex} layers, which provides syntax highligting and
some other options. Note that these do require appropriate installations of
\texttt{git}, \LaTeX, and the npm package \texttt{elm-oracle}.

Note that the bare minimum is \texttt{git}, \texttt{node.js}, \texttt{elm},
\texttt{elm-test}, and a text editor. Everything else just makes things easier.

Once you have those tools installed, it is as simple as:
\begin{enumerate}
  \begin{item}
    Clone the repository (to a local directory).
  \end{item}
  \begin{item}
    Run \texttt{elm package install -y} in the repository root.
  \end{item}
  \begin{item}
    Run \texttt{elm package install -y} in the \texttt{tests/} directory.
  \end{item}
  \begin{item}
    Run \texttt{elm reactor} from the repository root.
  \end{item}
  \begin{item}
    For tests, run \texttt{elm-reactor} from the \texttt{tests/} directory. You
    will need to specify a non-standard port. See \texttt{reactor.ps1} for
    details (you can run this directly on a Windows system with PowerShell).
  \end{item}
  \begin{item}
    Both of the above provide a URL on the console. Open it in your favorite web
    browser that supports WebGL.
  \end{item}
\end{enumerate}

Documentation links to the libraries used are available from the \texttt{elm
  reactor} page you open in a browser.

\theendnotes

\end{document}
