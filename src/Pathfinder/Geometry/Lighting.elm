module Pathfinder.Geometry.Lighting
    exposing
        ( Lighting
        , makeLighting
        , defaultLighting
        )

import Math.Vector4 as Vec4 exposing (Vec4, vec4)


type alias Lighting =
    { ambientLight : Vec4
    , diffuseLight : Vec4
    , specularLight : Vec4
    , ambientMaterial : Vec4
    , diffuseMaterial : Vec4
    , specularMaterial : Vec4
    , shininess : Float
    }


makeLighting : Vec4 -> Vec4 -> Vec4 -> Lighting
makeLighting a d s =
    { defaultLighting
        | ambientMaterial = a
        , diffuseMaterial = d
        , specularMaterial = s
    }


defaultLighting =
    { ambientLight = (vec4 0.6 0.6 0.6 1.0)
    , diffuseLight = (vec4 1.0 1.0 1.0 1.0)
    , specularLight = (vec4 1.0 1.0 1.0 1.0)
    , ambientMaterial = (vec4 1.0 0.0 1.0 1.0)
    , diffuseMaterial = (vec4 1.0 0.5 0.0 1.0)
    , specularMaterial = (vec4 0.5 0.8 0.0 1.0)
    , shininess = 10
    }
