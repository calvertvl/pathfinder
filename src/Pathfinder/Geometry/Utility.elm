module Pathfinder.Geometry.Utility
    exposing
        ( toVec3
        , toVec4
        , calcNormal
        , vec4mul
        , color
        , Vertex
        , Uniforms
        , Varyings
        )

import Math.Vector3 as Vec3 exposing (Vec3, vec3)
import Math.Vector4 as Vec4 exposing (Vec4, vec4)
import Math.Matrix4 as Mat4 exposing (Mat4)
import List exposing (drop, head)
import Color exposing (Color)


type alias Uniforms =
    { modelViewMatrix : Mat4
    , projectionMatrix : Mat4
    , lightPosition : Vec4
    , shininess : Float
    , normalMatrix : Mat4
    , ambientProduct : Vec4
    , diffuseProduct : Vec4
    , specularProduct : Vec4
    }


type alias Vertex =
    { vNormal : Vec4
    , vPosition : Vec4
    }


type alias Varyings =
    { fColor : Vec4
    }


toVec3 : Vec4 -> Vec3
toVec3 v =
    let
        a =
            Vec4.toRecord v

        x =
            a.x / a.w

        y =
            a.y / a.w

        z =
            a.z / a.w
    in
        vec3 x y z


toVec4 : Vec3 -> Vec4
toVec4 v =
    let
        a =
            Vec3.toRecord v
    in
        vec4 a.x a.y a.z 1


calcNormal : List Vec3 -> ( Int, Int, Int ) -> Vec3
calcNormal pts tri =
    let
        nth n ls =
            drop (n - 1) ls |> head

        zerovec =
            vec3 0 0 0

        ( a, b, c ) =
            tri

        v1 =
            Maybe.withDefault zerovec <| nth a pts

        v2 =
            Maybe.withDefault zerovec <| nth b pts

        v3 =
            Maybe.withDefault zerovec <| nth c pts

        d =
            Vec3.sub v2 v1

        e =
            Vec3.sub v3 v1
    in
        Vec3.cross d e
            |> Vec3.normalize

vec4mul : Vec4 -> Vec4 -> Vec4
vec4mul a b =
    let
        at =
            Vec4.toRecord a

        bt =
            Vec4.toRecord b
    in
        Vec4.fromTuple ( at.x * bt.x, at.y * bt.y, at.z * bt.z, at.w * bt.w )


color : Color -> Int -> Vec4
color rawColor alpha =
    let
        c =
            Color.toRgb rawColor
    in
        vec4
            (toFloat c.red / 255)
            (toFloat c.green / 255)
            (toFloat c.blue / 255)
            (toFloat alpha / 255)

