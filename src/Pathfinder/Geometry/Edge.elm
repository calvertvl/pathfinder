module Pathfinder.Geometry.Edge
    exposing
        ( edgeColor
        , edgeMesh
        )

import Math.Vector4 exposing (vec4)
import List exposing (map, map2)
import WebGL exposing (Mesh)


-- internal modules

import Pathfinder.Geometry.Utility exposing (toVec3, toVec4, calcNormal, Vertex)
import Pathfinder.Geometry.Lighting exposing (Lighting, makeLighting)
import Graph.Types as T exposing (PathNode, PathEdge)


edgeColor : T.EdgeState -> Lighting
edgeColor s =
    let
        a_d =
            (vec4 0.6 0.6 0.6 1.0)

        d_d =
            (vec4 1.0 0.5 0.5 1.0)

        s_d =
            (vec4 0.5 0.8 0.0 1.0)

        a_a =
            (vec4 1.0 0.0 0.0 1.0)

        d_a =
            (vec4 1.0 0.5 0.5 1.0)

        s_a =
            (vec4 0.8 0.5 0.0 1.0)

        a_p =
            (vec4 0.5 0.0 0.5 1.0)

        d_p =
            (vec4 1.0 0.5 0.0 1.0)

        s_p =
            (vec4 0.5 0.8 0.0 1.0)

        a_s =
            (vec4 0.0 0.0 1.0 1.0)

        d_s =
            (vec4 0.5 0.5 0.0 1.0)

        s_s =
            (vec4 0.5 0.8 0.0 1.0)
    in
        case s of
            T.DefaultEdge ->
                makeLighting a_d d_d s_d

            T.ShortestEdge ->
                makeLighting a_s d_s s_s

            T.ActiveEdge ->
                makeLighting a_a d_a s_a

            T.PendingEdge ->
                makeLighting a_p d_p s_p



{-
   I am using a basic hexagonal prism mesh for these.
   The axis of the prism is the Z axis.
-}


edgeMesh : Mesh Vertex
edgeMesh =
    let
        angle =
            Basics.degrees 60
                |> Debug.log "edgeMesh deg: "

        tb =
            Basics.sin angle
                |> Debug.log "edgeMesh sin: "

        lr =
            Basics.cos angle
                |> Debug.log "edgeMesh cos: "

        points =
            [ vec4 1 0 0 1
            , vec4 lr tb 0 1
            , vec4 -lr tb 0 1
            , vec4 -1 0 0 1
            , vec4 -lr -tb 0 1
            , vec4 lr -tb 0 1

            -- end back (0-5)
            , vec4 1 0 1 1
            , vec4 lr tb 1 1
            , vec4 -lr tb 1 1
            , vec4 -1 0 1 1
            , vec4 -lr -tb 1 1
            , vec4 lr -tb 1 1

            -- end front (6-11)
            , vec4 0 0 0 1 -- center bot (12)
            , vec4 0 0 1 1 -- center top (13)
            ]

        quad a b c d =
            [ ( a, b, c ), ( b, c, d ) ]

        indices =
            quad 0 1 6 7
                ++ quad 1 2 7 8
                ++ quad 2 3 8 9
                ++ quad 3 4 9 10
                ++ quad 4 5 10 11
                ++ quad 5 0 11 6
                ++ -- ends
                   [ ( 12, 1, 0 )
                   , ( 12, 1, 2 )
                   , ( 12, 2, 3 )
                   , ( 12, 3, 4 )
                   , ( 12, 4, 5 )
                   , ( 12, 5, 0 )
                   , ( 13, 6, 7 )
                   , ( 13, 7, 8 )
                   , ( 13, 8, 9 )
                   , ( 13, 9, 10 )
                   , ( 13, 10, 11 )
                   , ( 13, 11, 12 )
                   ]

        normals =
            map (calcNormal <| map (toVec3) points) indices
                |> map (toVec4)
    in
        WebGL.indexedTriangles (map2 (Vertex) normals points) indices
