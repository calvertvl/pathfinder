module Pathfinder.Geometry.Node
    exposing
        ( nodeColor
        , nodeMesh
        )

import Math.Vector4 exposing (vec4)
import List exposing (map, map2)
import WebGL exposing (Mesh)


-- internal modules

import Pathfinder.Geometry.Utility exposing (toVec3, toVec4, calcNormal, Vertex)
import Pathfinder.Geometry.Lighting exposing (Lighting, makeLighting)
import Graph.Types as T exposing (PathNode, PathEdge)


nodeColor : T.NodeState -> Lighting
nodeColor s =
    let
        a_d =
            (vec4 0.6 0.6 0.6 1.0)

        d_d =
            (vec4 1.0 0.5 0.5 1.0)

        s_d =
            (vec4 0.5 0.8 0.0 1.0)

        a_a =
            (vec4 1.0 0.0 0.0 1.0)

        d_a =
            (vec4 1.0 0.5 0.5 1.0)

        s_a =
            (vec4 0.8 0.5 0.0 1.0)

        a_p =
            (vec4 0.5 0.0 0.5 1.0)

        d_p =
            (vec4 1.0 0.5 0.0 1.0)

        s_p =
            (vec4 0.5 0.8 0.0 1.0)

        a_v =
            (vec4 0.1 0.1 0.1 1.0)

        d_v =
            (vec4 0.5 0.5 0.5 1.0)

        s_v =
            (vec4 0.5 0.8 0.0 1.0)

        a_c =
            (vec4 0.0 1.0 0.0 1.0)

        d_c =
            (vec4 0.5 0.5 0.0 1.0)

        s_c =
            (vec4 0.5 0.8 0.0 1.0)

        a_u =
            (vec4 1.0 0.0 0.0 1.0)

        d_u =
            (vec4 1.0 0.0 0.5 1.0)

        s_u =
            (vec4 0.5 0.8 0.0 1.0)

        a_s =
            (vec4 0.0 0.0 1.0 1.0)

        d_s =
            (vec4 0.5 0.5 0.0 1.0)

        s_s =
            (vec4 0.5 0.8 0.0 1.0)
    in
        case s of
            T.DefaultNode ->
                makeLighting a_d d_d s_d

            T.ActiveNode ->
                makeLighting a_a d_a s_a

            T.PendingNode ->
                makeLighting a_p d_p s_p

            T.VisitedNode ->
                makeLighting a_v d_v s_v

            T.CompareNode ->
                makeLighting a_c d_c s_c

            T.UpdateNode ->
                makeLighting a_u d_u s_u

            T.ShortestNode ->
                makeLighting a_s d_s s_s



{-
   I am using a diamond-shaped octahedron here
-}


nodeMesh : Mesh Vertex
nodeMesh =
    let
        points =
            [ vec4 0.5 0.5 0 1
            , vec4 0.5 -0.5 0 1
            , vec4 -0.5 -0.5 0 1
            , vec4 -0.5 0.5 0 1
            , vec4 0 0 0.5 1
            , vec4 0 0 -0.5 1
            ]

        indices =
            [ ( 0, 1, 4 )
            , ( 1, 2, 4 )
            , ( 2, 3, 4 )
            , ( 3, 0, 4 )
            , ( 0, 1, 5 )
            , ( 1, 2, 5 )
            , ( 2, 3, 5 )
            , ( 3, 0, 5 )
            ]

        normals =
            map (calcNormal <| map (toVec3) points) indices
                |> map (toVec4)
                |> Debug.log "nodeMesh normals: "
    in
        WebGL.indexedTriangles (map2 (Vertex) normals points) indices
