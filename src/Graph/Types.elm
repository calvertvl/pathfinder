module Graph.Types
    exposing
        ( PathNode
        , PathEdge
        , NodeState(..)
        , EdgeState(..)
        , NodeCost
        , distance
        , pNode
        )

import Graph exposing (NodeId)
import Math.Vector3 as Vec3 exposing (Vec3)
import Math.Matrix4 as Mat4 exposing (Mat4)


type alias NodeCost =
    Float


type alias PathNode =
    { x : Float
    , y : Float
    , z : Float
    , state : NodeState
    , cost : NodeCost
    , cheapest : List NodeId
    , transform : Mat4
    }


type alias PathEdge =
    { cost : Float
    , distance : Float
    , state : EdgeState
    , transform : Mat4
    }


type NodeState
    = DefaultNode
    | ActiveNode
    | PendingNode
    | VisitedNode
    | CompareNode
    | UpdateNode
    | ShortestNode


type EdgeState
    = DefaultEdge
    | ShortestEdge
    | ActiveEdge
    | PendingEdge


distance : PathNode -> PathNode -> Float
distance a b =
    List.map2 (-) [ a.x, a.y, a.z ] [ b.x, b.y, b.z ]
        |> List.map (\x -> x ^ 2)
        |> List.map abs
        |> List.sum
        |> sqrt


pNode : Float -> Float -> Float -> PathNode
pNode x y z =
    let
        infinity =
            1 / 0

        translation =
            Mat4.makeTranslate (Vec3.vec3 x y z)
                |> Mat4.scale3 0.5 0.5 0.5
    in
        PathNode x y z DefaultNode infinity [] translation
