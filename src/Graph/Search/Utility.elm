module Graph.Search.Utility
    exposing
        ( collapseSteps
        , collapse
        , findShortestPath
        , getGraphFromState
        , makeFinalState
        , toNodes
        , toEdges
        , Step(..)
        , edgesFromNodesWithin
        , infinity
        , updateNodeCost
        , resetNodeCost
        , sampleN
        , sampleG
        )

import List
    exposing
        ( foldr
        , concat
        , range
        , length
        , map
        , map2
        , map3
        , filterMap
        )
import Random
import Math.Vector3 as Vec3 exposing (Vec3, vec3)
import Math.Matrix4 as Mat4 exposing (Mat4)
import Graph
    exposing
        ( Graph
        , NodeId
        , Node
        , Edge
        )
import Graph.Types as T
    exposing
        ( PathNode
        , PathEdge
        , NodeCost
        , NodeState(..)
        , EdgeState(..)
        , distance
        )


type Step
    = InitialState (Graph PathNode PathEdge)
    | FinalState (List (List NodeId))
    | CurrentNode NodeId
    | Neighbors (List NodeId)
    | Compare NodeId NodeId
    | UpdateNodeCost NodeId NodeId NodeCost
    | MarkVisited NodeId
    | GeneralStep (List Step)


findShortestPath : List Step -> List (List NodeId)
findShortestPath ss =
    case ss of
        (FinalState pp) :: _ ->
            pp

        [] ->
            []

        _ :: st ->
            findShortestPath st


makeFinalState : NodeId -> NodeId -> Graph PathNode PathEdge -> Step
makeFinalState src ds g =
    let
        sourceFilter x =
            case x of
                [] ->
                    False

                n :: _ ->
                    n == src
    in
        pathsFrom g [ ds ] ds
            |> List.filter sourceFilter
            |> FinalState


pathsFrom : Graph PathNode PathEdge -> List NodeId -> NodeId -> List (List NodeId)
pathsFrom gr pts d =
    let
        q : List NodeId
        q =
            case Graph.get d gr of
                Just l ->
                    l.node.label.cheapest

                Nothing ->
                    []

        prepend x xs =
            x :: xs

        next : NodeId -> List (List NodeId)
        next d =
            pathsFrom gr (prepend d pts) d
    in
        case q of
            [] ->
                [ pts ]

            otherwise ->
                map (next) q
                    |> foldr (++) []


collapseSteps : Int -> List Step -> List Step
collapseSteps n s =
    let
        cond =
            (n > 0)
                && (length s >= 1)
                |> Debug.log "collapseSteps cond: "
    in
        if isCollapsable s then
            if cond then
                case s of
                    (InitialState g) :: s :: ss ->
                        collapseSteps (n - 1) <|
                            InitialState (collapse g s)
                                :: ss

                    otherwise ->
                        s
            else
                s
        else
            []


isCollapsable : List Step -> Bool
isCollapsable l =
    case l of
        (InitialState _) :: _ ->
            True

        otherwise ->
            False


collapse : Graph PathNode PathEdge -> Step -> Graph PathNode PathEdge
collapse g s =
    let
        updatePaths ps g =
            case ps of
                [] ->
                    g

                p :: pp ->
                    foldr (updateNodeState PendingNode) g p
                        |> updatePaths pp
    in
        case s of
            InitialState _ ->
                Debug.crash "Should never have two InitialState steps"

            FinalState ps ->
                updatePaths ps g

            CurrentNode n ->
                updateNodeState ActiveNode n g

            Neighbors ns ->
                foldr (updateNodeState PendingNode) g ns

            Compare na nb ->
                updateNodeState CompareNode na g
                    |> updateNodeState CompareNode nb

            UpdateNodeCost na nb nc ->
                updateNodeState UpdateNode nb g
                    |> updateNodeCost (Just na) nc nb

            MarkVisited n ->
                updateNodeState VisitedNode n g

            GeneralStep ss ->
                foldr (flip collapse) g ss


updateNodeState : NodeState -> NodeId -> Graph PathNode PathEdge -> Graph PathNode PathEdge
updateNodeState s n g =
    let
        setNodeState s nc =
            let
                update c =
                    let
                        oldNode =
                            c.node

                        oldLabel =
                            oldNode.label

                        newLabel =
                            { oldLabel | state = s }

                        newNode =
                            { oldNode | label = newLabel }
                    in
                        { c | node = newNode }
            in
                case nc of
                    Just c ->
                        Just <| update c

                    Nothing ->
                        Nothing
    in
        Graph.update n (setNodeState s) g


updateNodeCost : Maybe NodeId -> NodeCost -> NodeId -> Graph PathNode PathEdge -> Graph PathNode PathEdge
updateNodeCost sn sc n g =
    let
        setNodeCost sn sc nc =
            let
                update c =
                    let
                        oldNode =
                            c.node

                        oldLabel =
                            oldNode.label

                        isCurrentCheapest =
                            oldLabel.cost == sc

                        isNewCheapest =
                            case sn of
                                Just s ->
                                    List.member s oldLabel.cheapest

                                Nothing ->
                                    False

                        newLabel : PathNode
                        newLabel =
                            case ( isCurrentCheapest, sn, isNewCheapest ) of
                                ( False, Just jsn, _ ) ->
                                    { oldLabel
                                        | cost = sc
                                        , cheapest = [ jsn ]
                                    }

                                ( False, Nothing, _ ) ->
                                    { oldLabel
                                        | cost = sc
                                        , cheapest = []
                                    }

                                ( True, Just jsn, True ) ->
                                    { oldLabel | cheapest = jsn :: oldLabel.cheapest }

                                otherwise ->
                                    oldLabel

                        newNode =
                            { oldNode | label = newLabel }
                    in
                        { c | node = newNode }
            in
                case nc of
                    Just c ->
                        Just <| update c

                    Nothing ->
                        Nothing
    in
        Graph.update n (setNodeCost sn sc) g


resetNodeCost : Graph PathNode PathEdge -> Graph PathNode PathEdge
resetNodeCost g =
    let
        resetCost n =
            { n
                | cost = infinity
                , cheapest = []
            }
    in
        Graph.mapNodes (resetCost) g


getGraphFromState : Maybe Step -> Graph PathNode PathEdge
getGraphFromState m =
    let
        g s =
            case s of
                InitialState g ->
                    g

                otherwise ->
                    Graph.empty
    in
        case m of
            Just s ->
                g s

            Nothing ->
                Graph.empty


toNodes : List PathNode -> List (Node PathNode)
toNodes pp =
    let
        ns =
            range 1 <| length pp
    in
        map2 (Node) ns pp


toEdges : List NodeId -> List NodeId -> List PathEdge -> List (Edge PathEdge)
toEdges fs ts ls =
    map3 (Edge) fs ts ls



{-
   Here we need to process the list to find all nodes witha a distance.
   Procedural programming would use a nested loop.

   It is tricky here because we want the NodeId and some additional data, so
   effectively we must create the edges here.

   Also, note that we only nead to hit each pair once, so we can recursively
   process: all nodes before the current start node have already been seen.
   This only works for now because we ignore direction.
-}


edgesFromNodesWithin : Float -> List (Node PathNode) -> List (Edge PathEdge)
edgesFromNodesWithin d ns =
    let
        within : Float -> Node PathNode -> Node PathNode -> Maybe (Edge PathEdge)
        within d a b =
            let
                la =
                    a.label

                lb =
                    b.label

                ed =
                    distance la lb

                va =
                    vec3 la.x la.y la.z

                vb =
                    vec3 lb.x lb.y lb.z

                na =
                    Vec3.normalize va

                nb =
                    Vec3.normalize vb

                origin =
                    vec3 0 0 0

                lo =
                    Vec3.sub vb va |> Vec3.normalize

                axis =
                    Vec3.cross lo Vec3.k |> Vec3.normalize

                angle =
                    Basics.acos <| Vec3.dot lo Vec3.k

                transform =
                    Mat4.identity
                        |> Mat4.translate va
                        |> Mat4.rotate -angle axis
                        |> Mat4.scale3 0.1 0.1 ed
                        |> Debug.log "edge transform: "

                pe =
                    PathEdge ed ed DefaultEdge transform
            in
                if ed <= d then
                    Just <| Edge (a.id) (b.id) pe
                else
                    Nothing
    in
        case ns of
            p :: ps ->
                concat
                    [ filterMap (within d p) ps
                    , edgesFromNodesWithin d ps
                    ]

            [] ->
                []


infinity : Float
infinity =
    1 / 0


sampleN : List PathNode
sampleN =
    [ T.pNode 0.0 0.0 0.0
    , T.pNode 1.0 1.0 0.0
    , T.pNode 2.0 0.0 0.0
    , T.pNode 0.0 2.0 0.0
    , T.pNode 2.0 2.0 0.0
    ]


sampleG : Graph PathNode PathEdge
sampleG =
    let
        nodes =
            toNodes sampleN

        edges =
            edgesFromNodesWithin 2.0 nodes
    in
        Graph.fromNodesAndEdges nodes edges
