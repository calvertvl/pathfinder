module Graph.Search.Dijkstra
    exposing
        ( findShortestPath
        , getSteps
        )

import List
    exposing
        ( head
        , tail
        , filter
        , foldr
        , filterMap
        , map
        , sort
        )
import IntDict
import Dict
import Graph
    exposing
        ( Graph
        , NodeId
        , nodeIds
        )
import Graph.Search.Utility as Utility
    exposing
        ( Step(..)
        , infinity
        , updateNodeCost
        , resetNodeCost
        )
import Graph.Types
    exposing
        ( PathNode
        , PathEdge
        )


findShortestPath : NodeId -> NodeId -> Graph PathNode PathEdge -> List NodeId
findShortestPath source dest graph =
    let
        validSource =
            Graph.member source graph

        validDest =
            Graph.member dest graph
    in
        if validSource && validDest then
            getSteps source dest graph
                |> Utility.findShortestPath
                |> head
                |> Maybe.withDefault []
        else
            []


getSteps : NodeId -> NodeId -> Graph PathNode PathEdge -> List Step
getSteps source dest g =
    let
        nodes =
            nodeIds g
                |> filter (\x -> x /= source)

        newg =
            resetNodeCost g
                |> updateNodeCost Nothing 0.0 source

        init =
            InitialState newg

        next =
            CurrentNode source

        steps =
            doStep source dest newg nodes next
    in
        init :: steps


doStep : NodeId -> NodeId -> Graph PathNode PathEdge -> List NodeId -> Step -> List Step
doStep src ds g ns s =
    let
        next =
            case s of
                InitialState g ->
                    Debug.crash "Should always call doStep with CurrentStep, not InitialState"

                CurrentNode src ->
                    processCurrentNode g ns src

                otherwise ->
                    []

        nextnode =
            Maybe.withDefault 0 <|
                smallestNode newg ns

        newg =
            foldr (flip Utility.collapse) g next

        newns =
            List.filter (\x -> x /= nextnode) ns

        rest =
            if nextnode > 0 then
                doStep src ds newg newns (CurrentNode nextnode)
            else
                [ Utility.makeFinalState src ds newg ]
    in
        [ s ] ++ next ++ rest


processCurrentNode : Graph PathNode PathEdge -> List NodeId -> NodeId -> List Step
processCurrentNode g ns src =
    let
        sn =
            Graph.get src g

        ndict =
            case Graph.get src g of
                Just c ->
                    IntDict.union c.incoming c.outgoing

                Nothing ->
                    IntDict.empty

        nbs =
            filter (flip IntDict.member ndict) ns

        edgecost nb =
            case IntDict.get nb ndict of
                Just e ->
                    e.cost

                Nothing ->
                    0.0

        nodecost nc =
            case Graph.get nc g of
                Just c ->
                    c.node.label.cost

                Nothing ->
                    1 / 0

        sncost =
            case sn of
                Just n ->
                    n.node.label.cost

                Nothing ->
                    0.0

        update s d =
            let
                oc =
                    nodecost d

                nc =
                    sncost + edgecost d
            in
                if oc >= nc then
                    [ UpdateNodeCost s d nc ]
                else
                    []

        cnode : Graph PathNode PathEdge -> NodeId -> NodeId -> List Step
        cnode gr a b =
            [ Compare a b ]
                ++ (update a b)

        compared =
            map (cnode g src) nbs
                |> List.concat

        close =
            [ MarkVisited src ]
    in
        if List.isEmpty nbs then
            close
        else
            (Neighbors nbs) :: compared ++ close


smallestNode : Graph PathNode PathEdge -> List NodeId -> Maybe NodeId
smallestNode g ns =
    let
        nodes =
            filterMap (flip Graph.get g) ns

        costs =
            map (.node >> .label >> .cost) nodes

        minc =
            case List.minimum costs of
                Just m ->
                    m

                Nothing ->
                    1 / 0

        ismin n =
            (n.node.label.cost) == minc

        minn =
            filter (ismin) nodes
                |> map (.node >> .id)
                |> sort
                |> head
    in
        if minc == infinity then
            Nothing
        else
            minn
