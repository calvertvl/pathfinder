module Main exposing (..)

import Html exposing (Html, div, text, button, a)
import Html.Attributes exposing (width, height, style, href)
import Html.Events exposing (onClick)
import Time exposing (Time, millisecond)
import List exposing (map)
import AnimationFrame
import Math.Matrix4 as Mat4 exposing (Mat4)
import Math.Vector3 as Vec3 exposing (Vec3, vec3)
import Math.Vector4 as Vec4 exposing (Vec4, vec4)
import Platform.Sub
import Task
import WebGL exposing (Shader, Entity)
import Graph as G exposing (Graph)


-- local libraries

import Graph.Types as T exposing (PathNode, PathEdge)
import Graph.Search.Utility as U exposing (Step)
import Graph.Search.Dijkstra as Dijkstra
import Pathfinder.Geometry.Node as Node
import Pathfinder.Geometry.Edge as Edge
import Pathfinder.Geometry.Lighting as Lighting
import Pathfinder.Geometry.Utility as Utility


main : Program Never Model Msg
main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


init : ( Model, Cmd Msg )
init =
    ( startingModel
    , Cmd.none
    )


type alias Model =
    { time : Float
    , t : Time
    , running : Bool
    , nodes : List PathNode
    , steps : List Step
    , currentStep : Int
    , increment : Int
    , mvm : Mat4
    , axis : Vec3
    }


startingModel : Model
startingModel =
    { time = 0
    , t = 0
    , running = False
    , nodes = U.sampleN
    , steps = Dijkstra.getSteps 1 5 U.sampleG
    , currentStep = 0
    , increment = 0
    , mvm = Mat4.identity
    , axis = Vec3.i
    }


type Msg
    = Start
    | Pause
    | StepForward
    | StepBackward
    | XAxis
    | YAxis
    | ZAxis
    | Tick Time
    | FrameDelay Time


view : Model -> Html Msg
view model =
    let
        runstate =
            case model.running of
                True ->
                    "time: " ++ toString model.t

                False ->
                    "state: paused"
    in
        div []
            [ div [] [ toolbar, webglCanvas model ]
            , text <| "Current " ++ runstate
            , div [] [ documentationLink ]
            ]


documentationLink : Html Msg
documentationLink =
    a [ href "paper.pdf" ]
        [ text "Paper describing this program" ]


toolbar : Html Msg
toolbar =
    div []
        [ button [ onClick Start ] [ text "Start" ]
        , button [ onClick Pause ] [ text "Pause" ]
        , button [ onClick XAxis ] [ text "X-Axis Rotation" ]
        , button [ onClick YAxis ] [ text "Y-Axis Rotation" ]
        , button [ onClick ZAxis ] [ text "Z-Axis Rotation" ]
        , button [ onClick StepBackward ] [ text "<- Step <-" ]
        , button [ onClick StepForward ] [ text "-> Step ->" ]
        ]


webglCanvas : Model -> Html Msg
webglCanvas m =
    WebGL.toHtml
        [ height 512, width 512, style [ ( "display", "block" ) ] ]
    <|
        entityHelper m


entityHelper : Model -> List Entity
entityHelper m =
    let
        graph =
            U.collapseSteps m.currentStep m.steps
                |> List.head
                |> U.getGraphFromState

        nodes =
            G.nodes graph

        edges =
            G.edges graph

        newent mesh mvm lit =
            let
                mm =
                    Mat4.mul
                        m.mvm
                        mvm
            in
                WebGL.entity
                    vertexShader
                    fragmentShader
                    mesh
                    (uniforms mm lit)

        nodent nc =
            let
                nlit =
                    nc.label.state
                        |> Node.nodeColor

                nmv =
                    nc.label.transform
            in
                newent
                    Node.nodeMesh
                    nmv
                    nlit

        edgent ec =
            let
                elit =
                    ec.label.state
                        |> Edge.edgeColor

                emv =
                    ec.label.transform
            in
                newent
                    Edge.edgeMesh
                    emv
                    elit

        nodeMeshes =
            map (nodent) nodes

        edgeMeshes =
            map (edgent) edges
    in
        edgeMeshes ++ nodeMeshes


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        mvm =
            model.mvm

        t =
            Basics.degrees 1

        axis =
            model.axis

        newmvm axis =
            case model.running of
                True ->
                    Mat4.rotate t axis mvm

                False ->
                    mvm

        nextstep =
            model.currentStep + 1

        prevstep =
            model.currentStep - 1

        graphat step =
            U.collapseSteps step
                (Debug.log "model steps: " model.steps)
                |> Debug.log "collapsed: "
                |> List.head
                |> Debug.log "head: "
                |> U.getGraphFromState
                |> Debug.log "newstep: "

        nextgraph =
            graphat nextstep

        prevgraph =
            graphat prevstep
    in
        case msg of
            Tick newTime ->
                let
                    nt =
                        case model.running of
                            True ->
                                model.t + 1

                            False ->
                                model.t
                in
                    ( { model
                        | t = nt
                        , mvm = newmvm axis
                      }
                    , Cmd.none
                    )

            FrameDelay newTime ->
                ( model, Cmd.none )

            Start ->
                ( { model | running = True }, Cmd.none )

            Pause ->
                ( { model | running = False }, Cmd.none )

            StepForward ->
                ( { model
                    | currentStep = nextstep
                  }
                , Cmd.none
                )

            StepBackward ->
                ( { model
                    | currentStep = prevstep
                  }
                , Cmd.none
                )

            XAxis ->
                ( { model
                    | axis = Vec3.i
                    , mvm = newmvm axis
                  }
                , Cmd.none
                )

            YAxis ->
                ( { model
                    | axis = Vec3.j
                    , mvm = newmvm axis
                  }
                , Cmd.none
                )

            ZAxis ->
                ( { model
                    | axis = Vec3.k
                    , mvm = newmvm axis
                  }
                , Cmd.none
                )


subscriptions : Model -> Sub Msg
subscriptions model =
    let
        framerate =
            60

        msPerFrame =
            1000 / framerate
    in
        Platform.Sub.batch
            [ Time.every (msPerFrame * millisecond) Tick
            , AnimationFrame.diffs FrameDelay
            ]


uniforms : Mat4 -> Lighting.Lighting -> Utility.Uniforms
uniforms mvm lit =
    { modelViewMatrix = mvm
    , projectionMatrix =
        Mat4.mul
            (Mat4.makePerspective 45 1 0.01 100)
            (Mat4.makeLookAt (vec3 0 0 5) (vec3 1 1 0) (vec3 0 1 0))
    , lightPosition = vec4 10 10 10 0
    , shininess = lit.shininess
    , normalMatrix = mvm
    , ambientProduct = Utility.vec4mul lit.ambientLight lit.ambientMaterial
    , diffuseProduct = Utility.vec4mul lit.diffuseLight lit.diffuseMaterial
    , specularProduct = Utility.vec4mul lit.specularLight lit.specularMaterial
    }


vertexShader : Shader Utility.Vertex Utility.Uniforms Utility.Varyings
vertexShader =
    [glsl|
         attribute  vec4 vPosition;
         attribute  vec4 vNormal;
         varying vec4 fColor;

         uniform vec4 ambientProduct, diffuseProduct, specularProduct;
         uniform mat4 modelViewMatrix;
         uniform mat4 projectionMatrix;
         uniform vec4 lightPosition;
         uniform float shininess;
         uniform mat4 normalMatrix;

         void main()
         {

             vec3 pos = -(modelViewMatrix * vPosition).xyz;
             vec3 light = lightPosition.xyz;
             vec3 L = normalize( light - pos );


             vec3 E = normalize( -pos );
             vec3 H = normalize( L + E );

             // Transform vertex normal into eye coordinates

             vec4 N4 = normalize( normalMatrix*vNormal);
             vec3 N = N4.xyz;

             // Compute terms in the illumination equation
             vec4 ambient = ambientProduct;

             float Kd = max( dot(L, N), 0.0 );
             vec4  diffuse = Kd*diffuseProduct;

             float Ks = pow( max(dot(N, H), 0.0), shininess );
             vec4  specular = Ks * specularProduct;

             if( dot(L, N) < 0.0 ) {
	               specular = vec4(0.0, 0.0, 0.0, 1.0);
             }

             gl_Position = projectionMatrix * modelViewMatrix * vPosition;

             fColor = ambient + diffuse +specular;
             fColor.a = 1.0;

         }
    |]


fragmentShader : Shader {} Utility.Uniforms Utility.Varyings
fragmentShader =
    [glsl|
         precision mediump float;

         varying vec4 fColor;

         void main()
         {
             gl_FragColor = fColor;
         }
    |]
